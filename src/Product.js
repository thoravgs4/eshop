import React  from "react";
import './Product.css'
function Product ( {id,title,price,rating, image}){

    return  (
        <div className="Product">
            <div className="product_information">
                <p>{title}</p>
                <p className="product_price">
                    <strong>$</strong>
                    <small>{price}</small></p>
                <div className="product_rating">
                    {Array(rating)
                    .fill()
                     .map((_,i)=>(
                    <p>⭐</p>
                ))}
                    </div>
        
            </div>
    
            <img src={image}  alt="" />
            <button >Add Basket</button>
        


        </div>
    )
}

export default Product;